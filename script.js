/*
1) Опишіть своїми словами що таке Document Object Model (DOM)

DOM - це весь документ у вигляді деревоподібної структури, де кожен елемент
(такий як параграф, заголовок, зображення тощо) є вузлом, а атрибути, текстовий контент і інші властивості цих елементів
- це вузлові атрибути.

2) Яка різниця між властивостями HTML-елементів innerHTML та innerText?

    innerHTML:
Дозволяє отримати або змінити HTML-вміст елемента, включаючи текст, теги та їх атрибути.
Використовується для вставки HTML-коду або роботи з HTML-елементами в більш важкий спосіб.

    innerText:
Дозволяє отримати або змінити тільки текстовий вміст елемента, без включення HTML-тегів або їх атрибутів.
Якщо використовувати innerText для зміни значення, то будуть замінені всі HTML-теги внутрішнього тексту на простий текст.
Використовується для вставки чистого тексту або отримання текстового вмісту елемента без обробки HTML.

3) Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Для звернення до елемента сторінки за допомогою JavaScript є декілька способів:

    document.getElementById():

    document.querySelector():

    document.getElementsByClassName():

    document.getElementsByTagName():

    document.querySelectorAll():
*/





const paragraphs = document.querySelectorAll('p');
    paragraphs.forEach((paragraph) => {
    paragraph.style.backgroundColor = '#ff0000';
});


    const optionsList = document.getElementById('optionsList');
    console.log(optionsList);


    const parentElement = optionsList.parentElement;
    console.log(parentElement);


    if (optionsList.hasChildNodes()) {
    const childNodes = optionsList.childNodes;
    childNodes.forEach((node) => {
    console.log('Node Name:', node.nodeName, '| Node Type:', node.nodeType);
});
}


    const testParagraphElement = document.querySelector('.testParagraph');
    if (testParagraphElement) {
    testParagraphElement.textContent = 'This is a paragraph';
}


    const mainHeaderElement = document.querySelector('.main-header');
    const nestedElements = mainHeaderElement.querySelectorAll('*');
    nestedElements.forEach((element) => {
    console.log(element);
    element.classList.add('nav-item');
});


    const sectionTitleElements = document.querySelectorAll('.section-title');
    sectionTitleElements.forEach((element) => {
    element.classList.remove('section-title');
});

